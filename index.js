// mysqldump -u <username> -p<password> <database-name>
import net from "net";
import cron from "node-cron";
import log from "@ajar/marker";
import { exec } from "child_process";
import DBs from "./config.json";

const dbsToBackup = JSON.parse(JSON.stringify(DBs));

const socket = net.createConnection({ port: 8124 }, () => {
    log.cyan(" ✨⚡  connected to the storage server!  ⚡✨ ");

    for (const dbName in dbsToBackup) {
        // schedule a task to backup the db every 30 seconds
        cron.schedule("*/30 * * * * *", () => {
            const task = exec(`docker exec mysql-db /usr/bin/mysqldump -u root -pqwerty ${dbName}`);
    
            const backupFileName = `${Math.round(Date.now() / 1000)}-${dbsToBackup[dbName]}`;

            task.stdout?.on("data", data => {
                socket.write(`${JSON.stringify({ data, fileName: backupFileName })}\n`);
            });
        });

        // console.log("running backup every 30 seconds");
    }
});

socket.on("end", () => { 
  log.yellow('✨ ⚡ disconnected from storage server ⚡ ✨');
 });

socket.on("error", err => log.error(err));
